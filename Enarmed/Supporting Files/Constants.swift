//
//  Constants.swift
//  Gaver
//
//  Created by Victor Alejandria on 25/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import Foundation

public struct Google {
    static let kGoogleMapsAPIKey = "AIzaSyBSlsn4bWBd1mxHwVkmTmlMRimCT5xI1zo"
}

public struct WebService {
    //MARK: General Server URL
    static let kServerBaseURL = "http://enarmed.administradorpixel.com/api/"
    static let kProductionBaseURL = "http://enarmed.administradorpixel.com/api/"
}

public struct Payment {
    
    static let kSandboxMerchantID = "monpfzzhjhfglccsaktx"
    static let kSandboxApiKey = "sk_f3a76384233f4d99ae6b8221a22bb7e8"
    
    static let kProductionMerchantID = "m7jcaqrflstmfkk0kgpt"
    static let kProductionApiKey = "pk_9a0beaf6bd004bfcbd70df4d9b8cd931"
    
}
