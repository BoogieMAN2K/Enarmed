//
//  BuyFullViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 15/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import IWCocoa
import SwiftyStoreKit
import KRProgressHUD

class BuyFullViewController: UIViewController {

	@IBOutlet weak var buyButton: UIButton!
	@IBOutlet weak var planPrice: UILabel!

	@IBAction func buyAction(_ sender: UIButton) {
		KRProgressHUD.show()
		SwiftyStoreKit.purchaseProduct("com.iwannadigital.enarmed_ios.full", quantity: 1, atomically: false) { result in
			KRProgressHUD.dismiss()
			switch result {
			case .success(let product):
				_ = Plan.buyFull(refPayment: "pago", amount: 17).observeNext(with: { (resultRegister) in
					if product.needsFinishTransaction {
						SwiftyStoreKit.finishTransaction(product.transaction)
					}
				})
				print("Purchase Success: \(product.productId)")
			case .error(let error):
				switch error.code {
				case .unknown: print("Unknown error. Please contact support")
				case .clientInvalid: print("Not allowed to make the payment")
				case .paymentCancelled: break
				case .paymentInvalid: print("The purchase identifier was invalid")
				case .paymentNotAllowed: print("The device is not allowed to make the payment")
				case .storeProductNotAvailable: print("The product is not available in the current storefront")
				case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
				case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
				case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
				}
			}
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		KRProgressHUD.show()
		SwiftyStoreKit.retrieveProductsInfo(["com.iwannadigital.enarmed.test", "com.iwannadigital.enarmed_ios.full"]) { result in

			KRProgressHUD.dismiss()
			if let product = result.retrievedProducts.first {
				let priceString = product.localizedPrice!
				print("Product: \(product.localizedDescription), price: \(priceString)")
				self.planPrice.text = priceString
			}
			else if let invalidProductId = result.invalidProductIDs.first {
				return Utilities.alertMessage(viewController: self, title: "Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
			}
			else {
				print("Error: \(result.error)")
			}
		}
	}
}
