//
//  InitialUserViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 9/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import EZSwiftExtensions

class InitialUserViewController: UIViewController {

    @IBOutlet weak var register: IconButton!
    @IBOutlet weak var login: IconButton!

    @IBAction func registerUser(_ sender: IconButton) {
        self.performSegue(withIdentifier: "RegisterSegue", sender: self)
    }
    
    @IBAction func loginUser(_ sender: IconButton) {
        self.performSegue(withIdentifier: "LoginSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
