//
//  LoginViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 9/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import ReactiveKit
import Bond
import KRProgressHUD
import LocalAuthentication

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var email: IconTextField!
    @IBOutlet weak var password: IconTextField!
    @IBOutlet weak var login: IconButton!
    @IBOutlet weak var forgotPassword: UIButton!
    @IBOutlet weak var register: UIButton!
    
    let MyKeychainWrapper = KeychainWrapper()
    var context = LAContext()
    
    @IBAction func loginUser(_ sender: IconButton) {
        email.resignFirstResponder()
        password.resignFirstResponder()
        
        let hasLoginKey = UserDefaults.standard.bool(forKey: "hasLoginKey")
        if hasLoginKey == false {
            UserDefaults.standard.setValue(self.email.text, forKey: "username")
        }
        MyKeychainWrapper.setObject(password.text, forKey:kSecValueData)
        MyKeychainWrapper.writeToKeychain()
        UserDefaults.standard.set(true, forKey: "hasLoginKey")
        UserDefaults.standard.synchronize()
        
        KRProgressHUD.show()
        let loginSignal = User.login(username: self.email.text!, password: self.password.text!)
        _ = loginSignal.observeNext { result in
            if result {
                let userSignal = User.info()
                _ = userSignal.observeNext(with: { _ in
                    KRProgressHUD.dismiss()
                    self.view.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
                })
            } else {
				KRProgressHUD.dismiss()
                Utilities.alertMessage(viewController: self, title: "Error iniciando sesión", message: "Usuario o contraseña incorrecta, intente nuevamente")
            }
        }
    }
    
    @IBAction func registerUser(_ sender: UIButton) {
        self.performSegue(withIdentifier: "NewUserSegue", sender: self)
    }
    
    @IBAction func recoverPassword(_ sender: UIButton) {
        self.performSegue(withIdentifier: "RecoverUserSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        combineLatest(email.reactive.text, password.reactive.text) { email, password in
            return !((email?.isEmpty)! && (password?.isEmpty)!)
            }.bind(to: login.reactive.isEnabled)
        
        let textfields: [UITextField] = [email, password]
        
        registerForAutoScroll(scrollView: scrollView, textFields: textfields)
    }
}
