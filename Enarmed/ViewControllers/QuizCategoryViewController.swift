//
//  QuizCategoryViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 24/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import KRProgressHUD
import IWCocoa

class QuizCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

	@IBOutlet weak var usersName: UILabel!
	@IBOutlet weak var usersSpeciality: UILabel!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var maxQuestionsPicker: UIPickerView!
	@IBOutlet weak var pickerConstraint: NSLayoutConstraint!

	var maxQuestions = 0
	var selectedMaxQuestions = 1
	var selectedCategory: Int!
	var selectedQuiz = Quiz()
	var quizes = [Quiz]()
	let reuseIdentifier = "quizCell"
	var numberOfQuestions = [Int]()
	var viewForPicker: UIView!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.tableView.delegate = self
		self.tableView.dataSource = self

		self.usersName.text = UserDefaults.standard.string(forKey: "name")
		self.usersSpeciality.text = UserDefaults.standard.string(forKey: "speciality")

		KRProgressHUD.show()
		_ = Quiz.byCategory(self.selectedCategory).observeNext(with: { (result) in
			self.quizes = result
			self.tableView.reloadData()
			KRProgressHUD.dismiss()
		})

	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		self.navigationBarColor = UIColor.init(hexString: "85cdfe")
		UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
	}

	func dismissView(sender: UIPickerView) {
		UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
			self.pickerConstraint.constant = 0
			let quizViewController = self.storyboard?.instantiateViewController(withIdentifier: "QuizView") as! QuizViewController
			quizViewController.selectedQuiz = self.selectedQuiz
			quizViewController.maxQuestions = self.selectedMaxQuestions
			quizViewController.questionNumber = 1
			self.viewForPicker.removeFromSuperview()
			self.presentVC(quizViewController)
			self.numberOfQuestions.removeAll(keepingCapacity: false)
			self.maxQuestions = 0
		})
	}

	func cancelView(sender: UIPickerView) {
		UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
			self.pickerConstraint.constant = 0
			self.viewForPicker.removeFromSuperview()
			self.view.layoutIfNeeded()
			self.numberOfQuestions.removeAll(keepingCapacity: false)
			self.maxQuestions = 0
		})
	}

	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}

	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return self.numberOfQuestions.count
	}

	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return self.numberOfQuestions[row].toString
	}

	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		self.selectedMaxQuestions = self.numberOfQuestions[row]
	}

	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return quizes.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
		let flashcard = self.quizes[indexPath.row]

		cell.textLabel?.text = flashcard.name

		return cell
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		_ = User.userPlan().observeNext(with: { (userPlan) in
			if self.quizes[indexPath.row].questions.count == 0 && userPlan.name == "free" {
				Utilities.alertMessage(viewController: self, title: "Felicidades",
				                       message: "Quiz terminado con éxito. Te invitamos a adquirir la versión premiun para disfrutar de mayor contenido")
			} else if self.quizes[indexPath.row].questions.count == 0 && userPlan.name != "free"  {
				Utilities.alertMessage(viewController: self, title: "No hay preguntas",
				                       message: "No hay mas preguntas disponibles por el momento, pronto dispondras de mas preguntas para mejorar tus conocimientos.")
			} else {
				KRProgressHUD.show()
				_ = Quiz.byId(self.quizes[indexPath.row].id!).observeNext(with: { (quiz) in
					KRProgressHUD.dismiss()

					self.selectedQuiz = quiz
					self.maxQuestions = userPlan.plan == 1 ? quiz.plan.freeQuestions :
						quiz.plan.premiunQuestions

					for number in 1..<self.maxQuestions + 1 {
						self.numberOfQuestions.append(number)
					}

					self.maxQuestionsPicker.reloadAllComponents()

					self.viewForPicker = UIView(x: 0, y: (self.view.window?.frame.height)!, w: (self.view.window?.frame.width)!, h: 50)
					UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
						let maxQuestionFrame = self.maxQuestionsPicker.frame
						self.maxQuestionsPicker.showsSelectionIndicator = true
						self.maxQuestionsPicker.delegate = self
						self.maxQuestionsPicker.dataSource = self
						let toolbar = UIToolbar()
						toolbar.barStyle = .default
						toolbar.sizeToFit()
						let barButton = UIBarButtonItem.init(title: "Seleccionar", style: .done, target: self, action: #selector(self.dismissView))
						let barSpace = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
						let barButton2 = UIBarButtonItem.init(title: "Cerrar", style: .done, target: self, action: #selector(self.cancelView))
						toolbar.setItems([barButton, barSpace, barButton2], animated: true)
						self.pickerConstraint.constant = 216
						self.viewForPicker.frame = CGRect(x: 0, y: maxQuestionFrame.y - (self.pickerConstraint.constant + 50),
						                                  w: self.viewForPicker.frame.width, h: self.viewForPicker.frame.height)
						self.viewForPicker.addSubview(toolbar)
						self.view.addSubview(self.viewForPicker)
						self.view.layoutIfNeeded()
					})
					
				})
			}
		})
	}
}
