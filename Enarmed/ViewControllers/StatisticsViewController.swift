//
//  StatisticsViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 25/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import UICircularProgressRing

class StatisticsViewController: UIViewController {

	@IBOutlet weak var username: UILabel!
	@IBOutlet weak var userSpeciality: UILabel!
	@IBOutlet weak var examProgress: UICircularProgressRingView!
	@IBOutlet weak var quizProgress: UICircularProgressRingView!
	@IBOutlet weak var flashcardProgress: UICircularProgressRingView!
	@IBOutlet weak var examCorrect: UILabel!
	@IBOutlet weak var examFailed: UILabel!
	@IBOutlet weak var quizCorrect: UILabel!
	@IBOutlet weak var quizFailed: UILabel!
	@IBOutlet weak var flashcardCorrect: UILabel!
	@IBOutlet weak var flashcardFailed: UILabel!

	override func viewDidLoad() {
		super.viewDidLoad()

		self.username.text = UserDefaults.standard.string(forKey: "name")
		self.userSpeciality.text = UserDefaults.standard.string(forKey: "speciality")

		_ = Statistics.get().observeNext(with: { (statistics) in
			self.examStatistics(statistics: statistics)
			self.quizStatistics(statistics: statistics)
			self.flashcardStatistics(statistics: statistics)
		})
	}

	func examStatistics(statistics: Statistics) {
		guard let examTrue = statistics.exam_true, let examFalse = statistics.exam_false, let examTruePercentage = statistics.exam_true_percentage, let examFalsePercentage = statistics.exam_false_percentage else { return }

		let examTotal = examFalsePercentage + examTruePercentage
		let temp = (examTruePercentage * 100) / examTotal

		self.examProgress.setProgress(value: CGFloat(temp), animationDuration: 2.0)
		self.examCorrect.text = examTrue.toString
		self.examFailed.text = examFalse.toString
	}

	func quizStatistics(statistics: Statistics) {
		guard let quizTrue = statistics.quiz_true, let quizFalse = statistics.quiz_false, let quizTruePercentage = statistics.quiz_true_percentage, let quizFalsePercentage = statistics.quiz_false_percentage else { return }

		let quizTotal = quizFalsePercentage + quizTruePercentage
		let temp = (quizTruePercentage * 100) / quizTotal

		self.quizProgress.setProgress(value: CGFloat(temp), animationDuration: 2.0)
		self.quizCorrect.text = quizTrue.toString
		self.quizFailed.text = quizFalse.toString
	}

	func flashcardStatistics(statistics: Statistics) {
		guard let flashcardTrue = statistics.flashcard_true, let flashcardFalse = statistics.flashcard_false, let flashcardTruePercentage = statistics.flashcard_true_percentage, let flashcardFalsePercentage = statistics.flashcard_false_percentage else { return }

		let flashcardTotal = flashcardFalsePercentage + flashcardTruePercentage
		let temp = (flashcardTruePercentage * 100) / flashcardTotal

		self.flashcardProgress.setProgress(value: CGFloat(temp), animationDuration: 2.0)
		self.flashcardCorrect.text = flashcardTrue.toString
		self.flashcardFailed.text = flashcardFalse.toString
	}
	
	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/

}
