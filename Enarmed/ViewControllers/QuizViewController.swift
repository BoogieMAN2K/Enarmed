//
//  QuizesViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import DLRadioButton
import IWCocoa
import KRProgressHUD
import ReactiveKit
import Bond

class QuizViewController: UIViewController {

	@IBOutlet weak var username: UILabel!
	@IBOutlet weak var userSpeciality: UILabel!
	@IBOutlet weak var question: UITextView!
	@IBOutlet weak var answer1: DLRadioButton!
	@IBOutlet weak var answer2: DLRadioButton!
	@IBOutlet weak var answer3: DLRadioButton!
	@IBOutlet weak var answer4: DLRadioButton!
	@IBOutlet weak var nextQuestion: IconButton!

	var selectedQuiz: Quiz!
	var selectedAnswer = Observable<Int>(0)
	var isValid: Bool!
	var answers = [UserAnswer]()
	var questionNumber: Int!
	var maxQuestions: Int!

	override func viewDidLoad() {
		super.viewDidLoad()

		self.username.text = UserDefaults.standard.string(forKey: "name")
		self.userSpeciality.text = UserDefaults.standard.string(forKey: "speciality")

		self.selectedAnswer.map{ $0 > 0 }.bind(to: nextQuestion.reactive.isEnabled)

		let activeQuestion = selectedQuiz.questions[questionNumber - 1]
		self.question.text = activeQuestion.question
		self.answer1.setTitle(activeQuestion.answers[0].name, for: .normal)
		self.answer1.contentHorizontalAlignment = .left
		self.answer1.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
		self.answer2.setTitle(activeQuestion.answers[1].name, for: .normal)
		self.answer2.contentHorizontalAlignment = .left
		self.answer2.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
		self.answer3.setTitle(activeQuestion.answers[2].name, for: .normal)
		self.answer3.contentHorizontalAlignment = .left
		self.answer3.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
		self.answer4.setTitle(activeQuestion.answers[3].name, for: .normal)
		self.answer4.contentHorizontalAlignment = .left
		self.answer4.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);

	}

	@IBAction func selectedOption(_ sender: DLRadioButton) {
		if sender.isEqual(answer1) {
			self.selectedAnswer.value = selectedQuiz.questions[questionNumber - 1].answers[0].id
			self.isValid = selectedQuiz.questions[questionNumber - 1].answers[0].isValid
		}

		if sender.isEqual(answer2) {
			self.selectedAnswer.value = selectedQuiz.questions[questionNumber - 1].answers[1].id
			self.isValid = selectedQuiz.questions[questionNumber - 1].answers[1].isValid
		}

		if sender.isEqual(answer3) {
			self.selectedAnswer.value = selectedQuiz.questions[questionNumber - 1].answers[2].id
			self.isValid = selectedQuiz.questions[questionNumber - 1].answers[2].isValid
		}

		if sender.isEqual(answer4) {
			self.selectedAnswer.value = selectedQuiz.questions[questionNumber - 1].answers[3].id
			self.isValid = selectedQuiz.questions[questionNumber - 1].answers[3].isValid
		}
	}

	@IBAction func answerQuestion(_ sender: IconButton) {

		var answer = UserAnswer()
		answer.answer = String(self.selectedAnswer.value)
		answer.question = String(self.selectedQuiz.questions[questionNumber - 1].id!)
		answer.quiz = String(self.selectedQuiz.id!)
		answer.valid = self.isValid
		self.answers.append(answer)

		let message = QuizAnswerViewController()
		message.modalPresentationStyle = .overCurrentContext
		if answer.valid {
			message.header = "¡Excelente!"
			message.message = "Felicidades"
			message.messageImage = #imageLiteral(resourceName: "correcto_color")
			message.headerImage = #imageLiteral(resourceName: "excelente")
			message.feedbackMessage = selectedQuiz.questions[questionNumber - 1].feedback!
			message.constraint = 300
			
			self.presentVC(message)
		} else {
			message.header = "¡Oh, no! Fallaste"
			message.message = "Intentalo de nuevo"
			message.messageImage = #imageLiteral(resourceName: "reintentar_color")
			message.headerImage = #imageLiteral(resourceName: "exclamación")
			message.constraint = 150

			var answerNumber: Int = 1
			self.selectedQuiz.questions[questionNumber - 1].answers.forEach({ (answer) in
				if answer.isValid {
					switch answerNumber {
					case 1:
						let string = NSMutableAttributedString(string: answer.name!,
						                                       attributes:[NSForegroundColorAttributeName: UIColor.red])
						self.answer1.titleLabel?.attributedText = string
					case 2:
						let string = NSMutableAttributedString(string: answer.name!,
						                                       attributes:[NSForegroundColorAttributeName: UIColor.red])
						self.answer2.titleLabel?.attributedText = string
					case 3:
						let string = NSMutableAttributedString(string: answer.name!,
						                                       attributes:[NSForegroundColorAttributeName: UIColor.red])
						self.answer3.titleLabel?.attributedText = string
					case 4:
						let string = NSMutableAttributedString(string: answer.name!,
						                                       attributes:[NSForegroundColorAttributeName: UIColor.red])
						self.answer4.titleLabel?.attributedText = string
					default:
						break
					}
				}

				answerNumber += 1
			})

			let when = DispatchTime.now() + 2
			DispatchQueue.main.asyncAfter(deadline: when) {
				self.presentVC(message)
			}
		}

		message.view!.addTapGesture(action: { (tap) in
			message.dismissVC(completion: nil)

			if self.maxQuestions > self.questionNumber {
				let quizViewController = self.storyboard?.instantiateViewController(withIdentifier: "QuizView") as! QuizViewController
				quizViewController.selectedQuiz = self.selectedQuiz
				quizViewController.questionNumber = self.questionNumber! + 1
				quizViewController.answers = self.answers
				quizViewController.maxQuestions = self.maxQuestions
				self.presentVC(quizViewController)
			} else {
				KRProgressHUD.show()
				_ = Quiz.loadAnswers(answers: self.answers).observeNext(with: { (result) in
					KRProgressHUD.dismiss()
					if result {
						self.view.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
					}
				})
			}

		})
}

	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
