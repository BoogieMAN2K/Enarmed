//
//  TipsDetailViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 18/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit

class TipsDetailViewController: UIViewController {

	@IBOutlet weak var header: UILabel!
	@IBOutlet weak var textDetailTip: UITextView!

	var headerTip, detailTip: String!

	override func viewDidLoad() {
        super.viewDidLoad()

        self.header.text = headerTip
		self.textDetailTip.text = detailTip
		
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
