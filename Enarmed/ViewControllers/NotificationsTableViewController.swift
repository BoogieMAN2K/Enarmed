//
//  NotificationsTableViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 26/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import IWCocoa

class NotificationsTableViewController: UITableViewController {

	var notifications = [Notification]()

	override func viewDidLoad() {
		super.viewDidLoad()

		self.navigationItem.rightBarButtonItem = self.editButtonItem

		self.updateNotifications()
	}

	// MARK: - Table view data source

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return notifications.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath)
		let notification = self.notifications[indexPath.row]

		//cell.imageView?.af_setImage(withURL: URL.init(string: notification.image!)!)
		cell.textLabel?.text = notification.name!

		return cell
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			do {
				_ = try Notification.update(self.notifications[indexPath.row].id).observeNext(with: { _ in })
				[indexPath].forEach({ (indexPath) in
					self.notifications.remove(at: indexPath.row)
					tableView.deleteRows(at: [indexPath], with: .fade)
				})
			} catch {
				Utilities.alertMessage(viewController: self, title: "Error de comunicación", message: "No ha sido posible eliminar la notificación por un error interno, por favor intente nuevamente.")
			}
		}
	}

	func updateNotifications() {
		do {
			_ = try Notification.new().observeNext(with: { (notifications) in
				self.notifications = notifications
				self.tableView.dataSource = self
				self.tableView.delegate = self
				self.tableView.reloadData()
			})
		} catch {
			Utilities.alertMessage(viewController: self, title: "Error actualizando notificaciones", message: "Ha ocurrido un error mientras se actualizaban las notificaciones, salga de la ventana y vuelva a intentar")
		}
	}

	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
