//
//  FlashcardViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import ReactiveKit
import Bond
import KRActivityIndicatorView
import Cosmos
import EZSwiftExtensions

class FlashcardViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var headerFlashcard: UILabel!
    @IBOutlet weak var categoryFlashcard: UILabel!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var pdfButton: IconButton!
    @IBOutlet weak var imagesButton: IconButton!
    @IBOutlet weak var audioButton: IconButton!
    @IBOutlet weak var videoButton: IconButton!
	@IBOutlet weak var rating: CosmosView!
	@IBOutlet weak var calificateFlashcard: UIButton!
    
    var categoryHeader = Observable<String>("")
    var flashcard = Observable<Flashcard>(Flashcard())
    var pdfURL = Observable<URL>(URL.init(string: "http://www.example.com")!)
    var imagesURL = Observable<URL>(URL.init(string: "http://www.example.com")!)
    var audiosURL = Observable<URL>(URL.init(string: "http://www.example.com")!)
    var videosURL = Observable<URL>(URL.init(string: "http://www.example.com")!)
	let activityIndicator = KRActivityIndicatorView(style: .gradationColor(head: UIColor.init(hexString: "1C6EB4")!,
	                                                                       tail: .white))
    
	@IBAction func calificateAction(_ sender: UIButton) {
		_ = Flashcard.rate(self.flashcard.value.id!, stars: rating.rating.toInt).observeNext(with: { _ in })
	}

    @IBAction func playPDF(_ sender: Any) {
        let request = URLRequest.init(url: self.pdfURL.value)
        self.webView.loadRequest(request)
    }
    
    @IBAction func playImages(_ sender: Any) {
        let request = URLRequest.init(url: self.imagesURL.value)
        self.webView.loadRequest(request)
    }
    
    @IBAction func playAudio(_ sender: Any) {
        let request = URLRequest.init(url: self.audiosURL.value)
        self.webView.loadRequest(request)
    }
    
    @IBAction func playVideo(_ sender: Any) {
        let request = URLRequest.init(url: self.videosURL.value)
        self.webView.loadRequest(request)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
		self.headerFlashcard.text = UserDefaults.standard.string(forKey: "name")
		self.categoryFlashcard.text = self.flashcard.value.name
		self.activityIndicator.frame = CGRect(x: self.webView.bounds.width/2 - self.activityIndicator.bounds.width/2, y: 20,
		                                      width: self.activityIndicator.frame.width,
		                                      height: self.activityIndicator.frame.height)
		self.activityIndicator.hidesWhenStopped = true
		self.activityIndicator.isLarge = true
		self.webView.addSubview(self.activityIndicator)

		_ = self.webView.pageLoaded.observeNext { (loading) in
            if loading {
				self.activityIndicator.startAnimating()
			}
        }

        _ = self.webView.pageFinishLoaded.observeNext { (endLoading) in
            if endLoading {
				self.activityIndicator.stopAnimating()
            }
        }
        
        _ = flashcard.observeNext { (result) in
            if result.id != nil {
				_ = Flashcard.byId(result.id).observeNext(with: { _ in })
                result.files.forEach({ (flashcardFile) in
                    guard let url = URL.init(string: flashcardFile.filepath) else { return }
                    switch flashcardFile.filetype.lowercased() {
                    case "pdf":
                        self.pdfURL.value = url
                        let request = URLRequest.init(url: self.pdfURL.value)
                        self.webView.loadRequest(request)
                    case "audio":
                        self.audiosURL.value = url
                    case "video":
                        self.videosURL.value = url
                    case "imagen":
                        self.imagesURL.value = url
                    default:
                        break
                    }
                })
            }

			let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
			longPress.allowableMovement = 100
			longPress.minimumPressDuration = 0.3
			longPress.delegate = self
			longPress.delaysTouchesBegan = true
			longPress.delaysTouchesEnded = true
			longPress.cancelsTouchesInView = true
			self.webView.addGestureRecognizer(longPress)
			self.webView.scrollView.addGestureRecognizer(longPress)
        }

        _ = self.pdfButton.reactive.isEnabled.bind(signal: { self.pdfURL.map { return $0 != URL.init(string: "http://www.example.com") } }())
        _ = self.imagesButton.reactive.isEnabled.bind(signal: { self.imagesURL.map { return $0 != URL.init(string: "http://www.example.com")! } }())
        _ = self.audioButton.reactive.isEnabled.bind(signal: { self.audiosURL.map { return $0 != URL.init(string: "http://www.example.com")! } }())
        _ = self.videoButton.reactive.isEnabled.bind(signal: { self.videosURL.map { return $0 != URL.init(string: "http://www.example.com")! } }())
	}

	@objc func handleLongPress() {
		// No hacemos nada es solo para evitar el copiar y pegar en iOS.
	}

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

		_ = Flashcard.recoverValue(self.flashcard.value.id!).observeNext(with: { (result) in
			self.rating.rating = result.toDouble
		})
        
		self.navigationBarColor = UIColor.init(hexString: "85cdfe")
		UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
