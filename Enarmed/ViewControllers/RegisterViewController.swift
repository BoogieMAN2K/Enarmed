//
//  RegisterViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 9/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import ReactiveKit
import Bond
import KRProgressHUD

class RegisterViewController: BaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var name: IconTextField!
    @IBOutlet weak var email: IconTextField!
    @IBOutlet weak var phone: IconTextField!
    @IBOutlet weak var birthday: IconTextField!
    @IBOutlet weak var speciality: IconTextField!
    @IBOutlet weak var password: IconTextField!
    @IBOutlet weak var retryPassword: IconTextField!
    @IBOutlet weak var acceptTerms: UISwitch!
    @IBOutlet weak var register: IconButton!
    @IBOutlet weak var login: UIButton!

	var specialities = [Speciality]()
	var selectedSpeciality = 0
	var picker = UIPickerView()

	func datePickerValueChanged(sender: UIDatePicker) {
		self.activeField.text = sender.date.toString(format: "dd/MM/yyyy")
	}

	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}

	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return self.specialities.count
	}

	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return self.specialities[row].name
	}

	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		self.speciality.text = self.specialities[row].name
		self.selectedSpeciality = self.specialities[row].id
	}

	func setupDatePicker(_ textField: UITextField) {
		let picker: UIDatePicker = UIDatePicker()
		picker.datePickerMode = UIDatePickerMode.date
		textField.inputView = picker
		picker.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
	}

	func setupPicker(picker: UIPickerView, textField: UITextField) {
		picker.showsSelectionIndicator = true
		picker.delegate = self
		picker.dataSource = self
		let toolbar = UIToolbar()
		toolbar.barStyle = .default
		toolbar.sizeToFit()
		let barButton = UIBarButtonItem.init(title: "Seleccionar", style: .done, target: self, action: #selector(dismissView))
		toolbar.setItems([barButton], animated: true)
		textField.inputAccessoryView = toolbar
		textField.inputView = picker
	}

	func dismissView(sender: UIPickerView) {
		self.activeField.resignFirstResponder()
	}

    @IBAction func registerUser(_ sender: IconButton) {
        if (email.validateEmail().isValid && phone.validatePhoneNumber().isValid) {
            var user = User()
            user.name = self.name.text!
            user.email = self.email.text!
            user.phone = self.phone.text!
            user.birthday = self.birthday.text!
            user.speciality = self.selectedSpeciality
            user.password = self.password.text!
            
            KRProgressHUD.show()
            let userSignal = User.create(user: user)
            _ = userSignal.observeNext(with: { (result) in
                KRProgressHUD.dismiss()
                if (result.0) {
					Utilities.alertMessage(viewController: self, title: "Datos Guardados", message: "Se ha registrado el usuario correctamente", withCompletionHandler: {
							self.performSegue(withIdentifier: "LoginSegue", sender: self)
					})
                } else {
                    Utilities.alertMessage(viewController: self, title: "Ha ocurrido un error", message: "Ha ocurrido un error mientras se guardaba la información, por favor intente nuevamente")
                }
            })
        } else {
            Utilities.alertMessage(viewController: self, title: "Revise sus datos", message: "Por favor revise la información.")
        }
    }
    
    @IBAction func loginUser(_ sender: UIButton) {
        self.performSegue(withIdentifier: "LoginSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let textFields: [UITextField] = [name, email, phone, birthday, speciality, password, retryPassword]
        registerForAutoScroll(scrollView: self.scrollView, textFields: textFields)
        
        let firstEval = combineLatest(email.reactive.text, name.reactive.text, phone.reactive.text, birthday.reactive.text, speciality.reactive.text, acceptTerms.reactive.isOn) { email, name, phone, birthday, speciality, acceptTerms in
            return !((email?.isEmpty)! && (name?.isEmpty)! && (phone?.isEmpty)! && (birthday?.isEmpty)! && (speciality?.isEmpty)!)
            }.flatMap { return $0 }
        
        combineLatest(self.password.reactive.text, self.retryPassword.reactive.text, firstEval) {password, retry, firstGroup in
            return !((password?.isEmpty)! && (retry?.isEmpty)!) && (password == retry) && firstGroup
            }.bind(to: self.register.reactive.isEnabled)

		setupDatePicker(self.birthday)

		_ = Speciality.all().observeNext(with: { (result) in
			self.specialities = result
		})

		setupPicker(picker: picker, textField: self.speciality)
	}
}
