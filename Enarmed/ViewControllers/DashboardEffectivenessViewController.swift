//
//  DashboardEfectivenessViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 17/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import UICircularProgressRing

class DashboardEffectivenessViewController: UIViewController {

	@IBOutlet weak var usersName: UILabel!
	@IBOutlet weak var usersSpeciality: UILabel!
	@IBOutlet weak var progressValid: UICircularProgressRingView!
	@IBOutlet weak var progressEffectiveness: UICircularProgressRingView!

	override func viewDidLoad() {
        super.viewDidLoad()

		self.usersName.text = UserDefaults.standard.string(forKey: "name")!
		self.usersSpeciality.text = UserDefaults.standard.string(forKey: "speciality")!

		_ = DashboardEffectiveness.all().observeNext(with: { (result) in
			self.progressValid.setProgress(value: CGFloat(result.promValid.toCGFloat), animationDuration: 1.0)
			self.progressEffectiveness.setProgress(value: CGFloat(result.promEffectiveness.toCGFloat), animationDuration: 1.0)
		})
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
