//
//  ProfileViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 11/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import ReactiveKit
import Bond
import KRProgressHUD

class ProfileViewController: BaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var profilePicture: ProfilePictures!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var update: IconButton!
    @IBOutlet weak var name: IconTextField!
    @IBOutlet weak var email: IconTextField!
    @IBOutlet weak var phone: IconTextField!
    @IBOutlet weak var birthday: IconTextField!
    @IBOutlet weak var speciality: IconTextField!
    @IBOutlet weak var enarCode: IconTextField!

    var specialities = [Speciality]()
    var selectedSpeciality = 0
    var picker = UIPickerView()
    var userInfo = User()
    var oldMail = ""

    @IBAction func loadPicture(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = false
        present(picker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.profilePicture.image = pickedImage
        }

        dismiss(animated: true, completion: { self.name.becomeFirstResponder() })
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    @objc func datePickerValueChanged(sender: UIDatePicker) {
        self.activeField.text = sender.date.toString(format: "dd/MM/yyyy")
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.specialities.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.specialities[row].name
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.speciality.text = self.specialities[row].name
        self.selectedSpeciality = self.specialities[row].id
    }

    func setupDatePicker(_ textField: UITextField) {
        let picker: UIDatePicker = UIDatePicker()
        picker.datePickerMode = UIDatePickerMode.date
        textField.inputView = picker
        picker.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }

    func setupPicker(picker: UIPickerView, textField: UITextField) {
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        let barButton = UIBarButtonItem.init(title: "Seleccionar", style: .done, target: self, action: #selector(dismissView))
        toolbar.setItems([barButton], animated: true)
        textField.inputAccessoryView = toolbar
        textField.inputView = picker
    }

    @objc func dismissView(sender: UIPickerView) {
        self.activeField.resignFirstResponder()
    }

    @IBAction func updateProfile(_ sender: Any) {
        let updatePictureSignal = User.updateProfilePicture(picture: profilePicture.image!)

        KRProgressHUD.show()
        _ = updatePictureSignal.observeNext(with: { (result) in
			KRProgressHUD.dismiss()
            if result {
                var newUser = User()
                newUser.name = self.name.text!
                newUser.phone = self.phone.text!
                newUser.email = self.email.text!
                newUser.birthday = self.birthday.text!
                newUser.speciality = self.selectedSpeciality
                newUser.enarmCode = self.enarCode.text!
                do {
                    let updateProfileSignal = try User.update(user: newUser)
                    KRProgressHUD.show()
                    _ = updateProfileSignal.observeNext(with: { (result) in
                        KRProgressHUD.dismiss()
                        if (result) {
                            Utilities.alertMessage(viewController: self, title: "Datos Guardados", message: "Se han actualizado los datos del perfil correctamente", withCompletionHandler: {
                                if self.oldMail != self.email.text {
                                    Utilities.alertMessage(viewController: self, title: "Cambio de correo", message: "Se ha detectado un cambio de correo, es necesario volver a iniciar sesión", withCompletionHandler: {
                                        UserDefaults.resetStandardUserDefaults()

                                        self.view.window?.rootViewController = UIStoryboard.init(name: "User", bundle: Bundle.main).instantiateInitialViewController()
                                    })
                                }
                            })

                        } else {
                            Utilities.alertMessage(viewController: self, title: "Error", message: "Ha ocurrido un error mientras se guardaba la información, por favor intente nuevamente")
                        }
                    })
                } catch {
                    Utilities.alertMessage(viewController: self, title: "Error", message: "Ha ocurrido un error mientras se guardaba la información, por favor intente nuevamente")
                }
            }
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let textFields: [UITextField] = [name, email, phone, birthday, speciality, enarCode]

        registerForAutoScroll(scrollView: scrollview, textFields: textFields)

        combineLatest(email.reactive.text, name.reactive.text, phone.reactive.text, birthday.reactive.text, speciality.reactive.text, enarCode.reactive.text) { email, name, phone, birthday, speciality, enarCode in
            return !((email?.isEmpty)! && (name?.isEmpty)! && (phone?.isEmpty)! && (birthday?.isEmpty)! && (speciality?.isEmpty)! && (speciality?.isEmpty)!)
            }.bind(to: self.update.reactive.isEnabled)

        setupDatePicker(self.birthday)

        _ = Speciality.all().observeNext(with: { (result) in
            self.specialities = result
        })

        _ = User.info().observeNext { (result) in
            self.name.text = result.name
            self.email.text = result.email
            self.oldMail = result.email
            self.phone.text = result.phone
            self.birthday.text = result.birthday != nil ? Date.init(fromString: result.birthday!, format: "yyyy-MM-dd HH:mm:ss.000000")?.toString(format: "dd/MM/yyyy") : ""
			guard let specialityId = result.speciality else { return }
			guard let index = self.specialities.index(where: { $0.id == specialityId }) else { return }
			self.speciality.text = result.speciality != nil ? self.specialities[index].name! : ""
            self.enarCode.text = result.enarmCode ?? ""

			guard let image = result.image else { return }
			self.profilePicture.af_setImage(withURL: URL.init(string: image)!)
        }

        setupPicker(picker: picker, textField: self.speciality)

        _ = User.info().observeNext(with: { (result) in
            self.userInfo = result
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
