//
//  EvalAppViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 1/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import Cosmos
import IWCocoa
import KRProgressHUD

class EvalAppViewController: BaseViewController, UIPickerViewDataSource, UIPickerViewDelegate {

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var usefulApp: UITextField!
	@IBOutlet weak var usefulQuestions: UITextField!
	@IBOutlet weak var recommendApp: UITextField!
	@IBOutlet weak var comments: UITextField!
	@IBOutlet weak var rating: CosmosView!
	@IBOutlet weak var sendEval: IconButton!

	let picker = UIPickerView()
	let pickerOptions = ["Si", "No"]
	var selectedAnswer1, selectedAnswer2, selectedAnswer3: Int!
	var valorationAnswer = ValorationAnswer()

	override func viewDidLoad() {
		super.viewDidLoad()

		let textfields: [UITextField] = [usefulApp, usefulQuestions, recommendApp, comments]
		registerForAutoScroll(scrollView: self.scrollView, textFields: textfields)

		_ = ValorationAnswer.all().observeNext(with: { (result) in
			self.valorationAnswer = result
		})
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		setupPicker(textField: self.usefulQuestions)
		setupPicker(textField: self.usefulApp)
		setupPicker(textField: recommendApp)
	}

	func setupPicker(textField: UITextField) {
		picker.showsSelectionIndicator = true
		picker.delegate = self
		picker.dataSource = self
		let toolbar = UIToolbar()
		toolbar.barStyle = .default
		toolbar.sizeToFit()
		let barButton = UIBarButtonItem.init(title: "Seleccionar", style: .done, target: self, action: #selector(dismissView))
		toolbar.setItems([barButton], animated: true)
		textField.inputAccessoryView = toolbar
		textField.inputView = picker
	}

	func dismissView(sender: UIPickerView) {
		self.activeField.resignFirstResponder()
	}

	@IBAction func sendEvalAction(_ sender: Any) {
		KRProgressHUD.show()
		_ = ValorationAnswer.upload(useful: self.selectedAnswer1, accordingly: self.selectedAnswer2, recommends: self.selectedAnswer3, comments: self.comments.text!, rating: self.rating.rating.toInt).observeNext(with: { (result) in
			KRProgressHUD.dismiss()
			if result.0 {
				Utilities.alertMessage(viewController: self, title: "Comentarios Enviados", message: result.1) {
					self.popVC()
				}
			}
		})
	}

	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}

	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		switch self.activeField.tag {
		case 1:
			let val = self.valorationAnswer.useful.toJSON()
			return val.count
		case 2:
			let val = self.valorationAnswer.accordingly.toJSON()
			return val.count
		case 3:
			let val = self.valorationAnswer.recommends.toJSON()
			return val.count
		default:
			return 0
		}
	}

	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		switch self.activeField.tag {
		case 1:
			let val = self.valorationAnswer.useful.toJSON()
			return (val[(row + 1).toString]! as! String)
		case 2:
			let val = self.valorationAnswer.accordingly.toJSON()
			return (val[(row + 1).toString]! as! String)
		case 3:
			let val = self.valorationAnswer.recommends.toJSON()
			return (val[(row + 1).toString]! as! String)
		default:
			return String.init()
		}
	}

	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		switch self.activeField.tag {
		case 1:
			let val = self.valorationAnswer.useful.toJSON()
			self.activeField.text = (val[(row + 1).toString]! as! String)
			self.selectedAnswer1 = row
		case 2:
			let val = self.valorationAnswer.accordingly.toJSON()
			self.activeField.text = (val[(row + 1).toString]! as! String)
			self.selectedAnswer2 = row
		case 3:
			let val = self.valorationAnswer.recommends.toJSON()
			self.activeField.text = (val[(row + 1).toString]! as! String)
			self.selectedAnswer3 = row
		default:
			break
		}
	}


	/*
	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/

}
