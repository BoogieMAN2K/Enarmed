//
//  MainViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController, UITabBarControllerDelegate, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        moreNavigationController.delegate = self

		let tabBarControllerItems = self.tabBarController?.tabBar.items
		if let tabArray = tabBarControllerItems {
			let tabBarItem1 = tabArray[8]

			_ = ValorationAnswer.getValoration().observeNext(with: { (result) in
				tabBarItem1.isEnabled = !result
			})
		}}

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if (viewController.isKind(of: LogOutViewController.self)) {
            self.presentVC(UIStoryboard.init(name: "User", bundle: Bundle.main).instantiateInitialViewController()!)
            UserDefaults.resetStandardUserDefaults()
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(false, forKey: "hasLoginKey")
            
            return false
        }
        
        return true
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if (viewController.isKind(of: LogOutViewController.self)) {
            self.presentVC(UIStoryboard.init(name: "User", bundle: Bundle.main).instantiateInitialViewController()!)
            UserDefaults.resetStandardUserDefaults()
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set(false, forKey: "hasLoginKey")
        }
    }
    
}
