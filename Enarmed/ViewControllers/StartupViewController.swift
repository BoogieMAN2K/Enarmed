//
//  ViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 6/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import IWCocoa

class StartupViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: "hasLoginKey") {
            let keychainWrapper = KeychainWrapper()
            
            guard let password = keychainWrapper.object(forKey: "v_Data") as? String, let username = UserDefaults.standard.string(forKey: "username") else {
                self.view.window?.rootViewController = UIStoryboard.init(name: "User", bundle: Bundle.main).instantiateInitialViewController()
                
                return
            }
            
            let loginSignal = User.login(username: username, password: password)
            _ = loginSignal.observeNext(with: { (result) in
                if result {
                    let userSignal = User.info()
                    _ = userSignal.observeNext(with: { (user) in
                        
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(user.name, forKey: "name")
                        userDefaults.set(user.email, forKey: "email")
                        userDefaults.set(user.image, forKey: "image")
                        userDefaults.set(user.lastLogin, forKey: "lastLogin")
                        userDefaults.set(user.phone, forKey: "phone")
                        userDefaults.set(user.status.rawValue, forKey: "status")
						_ = Speciality.all().observeNext(with: { (result) in
							guard let specialityId = user.speciality else { return }
							guard let index = result.index(where: { $0.id == specialityId }) else { return }
							let speciality = result[index].name!
							userDefaults.set(speciality, forKey: "speciality")
						})
                   })
                    self.view.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
                } else {
                    self.view.window?.rootViewController = UIStoryboard.init(name: "User", bundle: Bundle.main).instantiateInitialViewController()
                }
            })
        } else {
            self.performSegue(withIdentifier: "UserSegue", sender: self)
        }
        
    }

}

