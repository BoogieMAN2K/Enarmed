//
//  FlashcardListViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond
import KRProgressHUD

class FlashcardListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
	@IBOutlet weak var usersName: UILabel!
	@IBOutlet weak var usersSpeciality: UILabel!
    @IBOutlet weak var tableView: UITableView!

	var selectedCategory: Int!
    var flashCards = [Flashcard]()
    var selectedFlashcard: Flashcard!
	var category: String!
    let reuseIdentifier = "flashcardCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self

		self.usersName.text = UserDefaults.standard.string(forKey: "name")
		self.usersSpeciality.text = self.category!
        
        let flashcardSignal = Flashcard.byCategory(self.selectedCategory)
        KRProgressHUD.show()
        _ = flashcardSignal.observeNext(with: { (result) in
            self.flashCards = result
            self.tableView.reloadData()
            KRProgressHUD.dismiss()
        })

}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
		self.navigationBarColor = UIColor.init(hexString: "85cdfe")
		UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination.isKind(of: FlashcardViewController.self)) {
            let destinationVC = segue.destination as! FlashcardViewController
            destinationVC.flashcard.value = self.selectedFlashcard
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flashCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        let flashcard = self.flashCards[indexPath.row]
        
        cell.textLabel?.text = flashcard.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedFlashcard = self.flashCards[indexPath.row]
        
        self.performSegue(withIdentifier: "FlashcardSegue", sender: self)
    }
}
