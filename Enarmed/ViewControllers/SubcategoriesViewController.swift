//
//  SubcategoriesViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 17/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import KRProgressHUD
import IWCocoa
import ReactiveKit
import Bond

private let reuseIdentifier = "categoriesCell"

class SubcategoriesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

	@IBOutlet weak var usersName: UILabel!
	@IBOutlet weak var usersSpeciality: UILabel!
	@IBOutlet weak var collectionView: UICollectionView!
	var categories = [Category]()
	var selectedCategory: Category!

	override func viewDidLoad() {
		super.viewDidLoad()

		self.usersName.text = UserDefaults.standard.string(forKey: "name")
		self.usersSpeciality.text = UserDefaults.standard.string(forKey: "speciality")

		let flowLayout = UICollectionViewFlowLayout.init()
		flowLayout.scrollDirection = .vertical
		flowLayout.itemSize = CGSize(width: 80, height: 80)
		flowLayout.minimumInteritemSpacing = 0.5
		flowLayout.minimumLineSpacing = 5.0
		self.collectionView?.collectionViewLayout = flowLayout

		// Register cell classes
		self.collectionView!.register(UINib.init(nibName: "CategoriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
		self.collectionView!.delegate = self
		self.collectionView!.dataSource = self
	}

	override func viewWillAppear(_ animated: Bool) {
		self.navigationBarColor = UIColor.init(hexString: "85cdfe")
		UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]

		guard let childrens = self.selectedCategory.childrens else { return }
		self.categories = childrens
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.destination.isKind(of: FlashcardListViewController.self)) {
			let destinationVC = segue.destination as! FlashcardListViewController
			destinationVC.selectedCategory = self.selectedCategory.id
			destinationVC.category = self.selectedCategory.name
		}

	}

	// MARK: UICollectionViewDataSource

	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return categories.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CategoriesCollectionViewCell
		let category = categories[indexPath.row]

		cell.categoryName.text = category.name
		let childrens = category.childrens ?? [Category]()
		let flashcards = (category.flashcard != nil) ? category.flashcard! : "0"
		if (!childrens.isEmpty) {
			cell.categoryItems.text = "\(childrens.count) Subs"
		} else {
			cell.categoryItems.text = "\(flashcards) Flashcards"
		}

		return cell
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		self.selectedCategory = self.categories[indexPath.row]
		let childrens = self.selectedCategory.childrens ?? [Category]()
		let flashcards = self.selectedCategory.flashcard

		if childrens.isEmpty && flashcards == "0" {
			Utilities.alertMessage(viewController: self, title: "Sin información", message: "No hay información en este momento")
			return
		}
		if childrens.isEmpty {
			self.performSegue(withIdentifier: "Themes", sender: self)
			return
		}
	}


}
