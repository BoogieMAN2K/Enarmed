//
//  QuizSubcategoriesViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 17/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond
import KRProgressHUD

private let reuseIdentifier = "categoriesCell"

class QuizSubcategoriesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

	@IBOutlet weak var usersName: UILabel!
	@IBOutlet weak var usersSpeciality: UILabel!
	@IBOutlet weak var collectionView: UICollectionView!
	var categories = [Category]()
	var selectedCategory: Category!

	override func viewDidLoad() {
		super.viewDidLoad()

		self.usersName.text = UserDefaults.standard.string(forKey: "name")
		self.usersSpeciality.text = UserDefaults.standard.string(forKey: "speciality")

		let flowLayout = UICollectionViewFlowLayout.init()
		flowLayout.scrollDirection = .vertical
		flowLayout.itemSize = CGSize(width: 80, height: 80)
		flowLayout.minimumInteritemSpacing = 0.5
		flowLayout.minimumLineSpacing = 5.0
		self.collectionView?.collectionViewLayout = flowLayout

		// Register cell classes
		self.collectionView!.register(UINib.init(nibName: "CategoriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
		self.collectionView!.delegate = self
		self.collectionView!.dataSource = self
	}

	override func viewWillAppear(_ animated: Bool) {
		self.navigationBarColor = UIColor.init(hexString: "85cdfe")
		UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.destination.isKind(of: QuizCategoryViewController.self)) {
			let destinationVC = segue.destination as! QuizCategoryViewController
			destinationVC.selectedCategory = self.selectedCategory.id
		}

	}

	// MARK: UICollectionViewDataSource

	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return categories.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CategoriesCollectionViewCell
		let category = categories[indexPath.row]

		cell.categoryName.text = category.name
		cell.categoryItems.text = String.init()

		return cell
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		self.selectedCategory = self.categories[indexPath.row]

		self.performSegue(withIdentifier: "QuizesByCategorySegue", sender: self)
	}

}
