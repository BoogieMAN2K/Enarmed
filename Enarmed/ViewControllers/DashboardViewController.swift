//
//  DashboardViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import UICircularProgressRing
import EZSwiftExtensions

class DashboardViewController: UIViewController {

    var allTips = [Tip]()
    var tipTimer: Timer!
    var selectedTip: Int!

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tips: UILabel!
    @IBOutlet weak var activityScrollView: UIScrollView!

    @IBAction func showTipsView(_ sender: Any) {
        self.performSegue(withIdentifier: "FullTipView", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.username.text = UserDefaults.standard.string(forKey: "name")

        _ = DashboardItem.all().observeNext(with: { result in
            var item = 0
            result.forEach({ (dashboardItem) in
                UIView.animate(withDuration: 1, animations: { 
                    let backColor = UIColor.random()
                    let progressView = UICircularProgressRingView.init(frame: CGRect(x: 10, y: CGFloat(20 + (120 * item)), w: 100, h: 100))
                    let categoryName = UILabel.init(frame: CGRect(x: 120, y: CGFloat(50 + (120 * item)),
                                                                  w: self.view.frame.width - 130, h: 30))
                    categoryName.textAlignment = .right
                    categoryName.backgroundColor = backColor
                    categoryName.textColor = UIColor.white
					categoryName.font = UIFont.boldSystemFont(ofSize: 17)
					categoryName.adjustsFontSizeToFitWidth = true
					categoryName.minimumScaleFactor = 0.5
                    self.activityScrollView.addSubview(progressView)
                    self.activityScrollView.addSubview(categoryName)
                    categoryName.text = "\(dashboardItem.category!)"
                    categoryName.roundCorners([.bottomLeft, .topLeft], radius: 8)
                    progressView.setProgress(value: CGFloat(dashboardItem.result.toCGFloat), animationDuration: 2.0)
                    progressView.innerRingColor = backColor
                    self.activityScrollView.contentSize = CGSize(width: 0, height: 130 * (item + 1))
                })
                item += 1
            })
        })

        _ = Tip.all().observeNext { (result) in
            self.allTips = result
            self.tipTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.randomNewTip), userInfo: nil, repeats: true)
            self.tips.isUserInteractionEnabled = true
        }
    }

    @objc func randomNewTip() {
		if !self.allTips.isEmpty {
			self.selectedTip = Int(arc4random_uniform(UInt32(self.allTips.count)))
			self.tips.text = self.allTips[self.selectedTip].description
		}
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: TipsDetailViewController.self) {
            let destinationVC = segue.destination as! TipsDetailViewController

            destinationVC.headerTip = self.allTips[selectedTip].name
            destinationVC.detailTip = self.allTips[selectedTip].description
        }
    }
}
