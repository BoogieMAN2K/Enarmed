//
//  DashboardThemesViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 17/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import UICircularProgressRing
import KRProgressHUD
import ReactiveKit
import Bond
import IWCocoa

class DashboardThemesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var usersName: UILabel!
	@IBOutlet weak var usersSpeciality: UILabel!
	@IBOutlet weak var progressDetail: UITableView!
	@IBOutlet weak var progressThemes: UILabel!
	@IBOutlet weak var progressQuizes: UILabel!
	@IBOutlet weak var progressFlashcards: UILabel!

	let tableViewSections = ["Temas", "Quiz", "Flashcards"]
	var tableThemesSection = [ThemeItem]()
	var tableQuizSection = [ThemeItem]()
	var tableFlashcardSection = [ThemeItem]()

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		switch section {
		case 0:
			return self.tableThemesSection.count
		case 1:
			return self.tableQuizSection.count
		case 2:
			return self.tableFlashcardSection.count
		default:
			return 0
		}
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

		switch indexPath.section {
		case 0:
			cell.textLabel?.text = self.tableThemesSection[indexPath.row].name
		case 1:
			cell.textLabel?.text = self.tableQuizSection[indexPath.row].name
		case 2:
			cell.textLabel?.text = self.tableFlashcardSection[indexPath.row].name
		default:
			cell.textLabel?.text = String.init()
		}

		return cell
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1)
		UIView.animate(withDuration: 0.3, animations: {
			cell.layer.transform = CATransform3DMakeScale(1.05, 1.05, 1)
		},completion: { finished in
			UIView.animate(withDuration: 0.1, animations: {
				cell.layer.transform = CATransform3DMakeScale(1, 1, 1)
			})
		})
	}

	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return self.tableViewSections[section]
	}

	func numberOfSections(in tableView: UITableView) -> Int {
		return self.tableViewSections.count
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		self.usersName.text = UserDefaults.standard.string(forKey: "name")!
		self.usersSpeciality.text = UserDefaults.standard.string(forKey: "speciality")!

		_ = DashboardThemes.all().observeNext(with: { (result) in
			Utilities.incrementLabel(label: self.progressThemes, to: result.themes)
			Utilities.incrementLabel(label: self.progressQuizes, to: result.quizes)
			Utilities.incrementLabel(label: self.progressFlashcards, to: result.flashcards)

			self.tableQuizSection = result.quizesDetail
			self.tableThemesSection = result.themesDetail
			self.tableFlashcardSection = result.flashcardDetail

			self.progressDetail.reloadData()
		})
		
	}
}
