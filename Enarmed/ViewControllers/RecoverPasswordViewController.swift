//
//  RecoverPasswordViewController.swift
//  EnarMed
//
//  Created by Victor Alejandria on 9/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa
import ReactiveKit
import Bond
import KRProgressHUD

class RecoverPasswordViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var email: IconTextField!
    @IBOutlet weak var recover: IconButton!
    @IBOutlet weak var register: UIButton!
    
    @IBAction func recoverUser(_ sender: IconButton) {
        let userSignal = User.recoverPassword(email: email.text!)
        KRProgressHUD.show()
        _ = userSignal.observeNext(with: { (result) in
            KRProgressHUD.dismiss()
            if result {
                Utilities.alertMessage(viewController: self, title: "Solicitud enviada", message: "Se le ha enviado un correo con instrucciones para recuperar su contraseña")
                self.popVC()
            } else {
                Utilities.alertMessage(viewController: self, title: "Error", message: "Ha ocurrido un error solicitando la recuperación de contraseña, por favor intente mas tarde")
            }
        })
    }
    
    @IBAction func registerUser(_ sender: UIButton) {
        self.performSegue(withIdentifier: "NewUserSegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        email.reactive.text.flatMap { (txt) -> Bool in
            return !(txt!.isEmpty)
        }.bind(to: recover.reactive.isEnabled)
        
		let textfields: [UITextField] = [email]

		registerForAutoScroll(scrollView: scrollView, textFields: textfields)
    }

}
