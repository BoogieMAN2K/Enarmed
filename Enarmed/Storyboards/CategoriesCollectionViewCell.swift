//
//  CategoriesCollectionViewCell.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit
import IWCocoa

class CategoriesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryItems: UILabel!
    @IBOutlet weak var categoryButton: IconButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
