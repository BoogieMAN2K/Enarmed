//
//  QuizAnswerViewController.swift
//  Enarmed
//
//  Created by Victor Alejandria on 20/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class QuizAnswerViewController: UIViewController {

	@IBOutlet weak var upperImage: UIImageView!
	@IBOutlet weak var headerMessage: UILabel!
	@IBOutlet weak var lowerImage: UIImageView!
	@IBOutlet weak var lowerMessage: UILabel!
	@IBOutlet weak var viewContainer: UIView!
	@IBOutlet weak var feedback: UITextView!
	@IBOutlet weak var dialogContraint: NSLayoutConstraint!

	var headerImage, messageImage: UIImage!
	var header, message, feedbackMessage: String!
	var constraint: CGFloat!

	override func viewDidLoad() {
        super.viewDidLoad()
		self.upperImage.image = self.headerImage
		self.lowerImage.image = self.messageImage
		self.headerMessage.text = self.header
		self.lowerMessage.text = self.message
		self.feedback.text = self.feedbackMessage
		self.dialogContraint.constant = self.constraint
    }


	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		self.viewContainer.roundCorners(.allCorners, radius: 20)
	}
}
