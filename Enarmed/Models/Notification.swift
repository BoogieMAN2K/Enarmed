//
//  Notification.swift
//  Enarmed
//
//  Created by Victor Alejandria on 30/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

final class Notification: FullResponse {

	var id: Int!
	var name: String!
	var read: Bool!

	override func mapping(map: Map) {
		id <- map["id"]
		name <- map["description"]
		read <- map["read"]
	}

	static func all() -> Signal<[Notification], NSError> {
		let notificationSignal = Services.requestArray(url: "notification/all", method: .get,
		                                               returnType: [Notification.self])

		return notificationSignal.map { $0.1 }
	}

	static func new() throws -> Signal<[Notification], NSError> {
		let notificationSignal = Services.requestArray(url: "notification/new", method: .get,
		                                          returnType: [Notification.self])

		return notificationSignal.map { $0.1 }
	}

	static func update(_ notificationId: Int) throws -> Signal<Notification, NSError> {
		let notificationSignal = try IWannaServices.request(url: "notification/update/\(notificationId)", method: .post,
		                                                headers: ["Authorization": "Bearer \(User().userToken!)"])

		return notificationSignal.map { self.result(response: $0) }

	}

}
