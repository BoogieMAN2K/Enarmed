//
//  DashboardThemes.swift
//  Enarmed
//
//  Created by Victor Alejandria on 17/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

final class DashboardThemes: FullResponse {

	var themes: Int!
	var quizes: Int!
	var flashcards: Int!
	var themesDetail: [ThemeItem]!
	var quizesDetail: [ThemeItem]!
	var flashcardDetail: [ThemeItem]!

	override func mapping(map: Map) {
		themes <- map["amountThemes"]
		quizes <- map["amountQuiz"]
		flashcards <- map["amountFlashCard"]
		themesDetail <- map["themesDetail"]
		quizesDetail <- map["quizDetail"]
		flashcardDetail <- map["flashCardDetail"]
	}

	static func all() -> Signal<DashboardThemes, NSError> {
		let dashboardSignal = try! IWannaServices.request(url: "account/dashboard/themes", method: .get,
		                                                  headers: ["Authorization": "Bearer \(User().userToken!)"])

		return dashboardSignal.map { result in
			self.result(response: result)
		}
	}

}

