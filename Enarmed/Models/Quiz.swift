//
//  Quiz.swift
//  Enarmed
//
//  Created by Victor Alejandria on 24/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa
import Alamofire

final class Quiz: FullResponse {
	var id: Int!
	var name: String!
	var description: String!
	var questions: [Question]!
	var plan: Plan!

	override func mapping(map: Map) {
		id <- map["quiz.id"]
		name <- map["quiz.name"]
		description <- map["quiz.description"]
		questions <- map["questions"]
		plan <- map["quiz.plan"]
	}

	static func all() -> Signal<[Quiz], NSError> {
		let quizSignal = Services.requestArray(url: "quiz/all", method: .get, returnType: [Quiz.self])

		return quizSignal.map { $0.1 }
	}

	static func byId(_ id: Int) -> Signal<Quiz, NSError> {
		let quizSignal = Services.request(url: "quiz/detail/\(id)", method: .get, returnType: Quiz.self)

		return quizSignal.map { $0.1 }
	}

	static func byCategory(_ id: Int) -> Signal<[Quiz], NSError> {
		let quizSignal = Services.requestArray(url: "quiz/category/\(id)", method: .get, returnType: [Quiz.self])

		return quizSignal.map { $0.1 }
	}

	static func loadAnswers(answers: [UserAnswer]) -> Signal<Bool, NSError> {
		let quizSignal = Services.requestJSON(url: "quiz/load",
		                                      parameters: answers,
		                                      method: .post,
		                                      headers: ["Content-Type":"application/json", "Authorization": "Bearer \(User().userToken!)"],
		                                      returnType: FullResponse.self)

		return quizSignal.map { $0.0 }
	}


}
