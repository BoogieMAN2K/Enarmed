//
//  FlashcardFile.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct FlashcardFile: Mappable {
    var id: Int!
    var filename: String!
    var filepath: String!
    var fileurl: String!
    var filetype: String!
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        filename <- map["fileName"]
        filepath <- map["filePath"]
        fileurl <- map["fileUrl"]
        filetype <- map["fileType"]
    }
    
    static func byId(id: Int) throws -> Signal<FlashcardFile, NSError> {
        let flashcardFileSignal = Services.request(url: "flash-card/file/\(id)", method: .get, returnType: FlashcardFile.self)
        
        return flashcardFileSignal.map { $0.1 }
    }
    
}
