//
//  Answer.swift
//  Enarmed
//
//  Created by Victor Alejandria on 24/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Answer: Mappable {
	var id: Int!
	var name: String!
	var isValid: Bool!

	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		id <- map["id"]
		name <- map["name"]
		isValid <- map["valid"]
	}
}
