//
//  ValorationAnswer.swift
//  Enarmed
//
//  Created by Victor Alejandria on 2/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct ValorationAnswer: Mappable {
	var useful: Valuation!
	var accordingly: Valuation!
	var recommends: Valuation!

	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		useful <- map["userful"]
		accordingly <- map["accordingly"]
		recommends <- map["recommends"]
	}

	static func all() -> Signal<ValorationAnswer, NSError> {
		let valorationSignal = Services.request(url: "valoration/answer", method: .get, returnType: ValorationAnswer.self)

		return valorationSignal.map { $0.1 }
	}

	static func upload(useful: Int, accordingly: Int, recommends: Int, comments: String, rating: Int) ->Signal<(Bool, String), NSError> {
		let parameters = ["userful": useful, "themesAccordingly": accordingly, "recommends": recommends, "observation": comments, "valoration": rating] as [String: Any]


		let valorationSignal = try! IWannaServices.request(url: "set-valoration", parameters: parameters, method: .post,
		                                              headers: ["Content-Type":"application/json", "Authorization": "Bearer \(User().userToken!)"])

		return valorationSignal.map {
			($0.response["status"].intValue == 200, $0.response["message"].stringValue)
		}
	}

	static func getValoration() -> Signal<Bool, NSError> {
		let valorationSignal = try! IWannaServices.request(url: "valoration/answer", method: .get,
		                                              headers: ["Content-Type":"application/json", "Authorization": "Bearer \(User().userToken!)"])

		return valorationSignal.map { $0.response["status"].intValue == 200 }
	}

}

struct Valuation: Mappable {
	var val1: String!
	var val2: String!
	var val3: String!

	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		val1 <- map["1"]
		val2 <- map["2"]
		val3 <- map["3"]
	}
}
