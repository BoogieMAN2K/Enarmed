//
//  GenericResponse.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/24/16.
//  Copyright © 2016 ISMCenter. All rights reserved.
//
import ObjectMapper
import IWCocoa

class FullResponse: Mappable {
    
    var status: Int!
    var message: String!
    
    required init() {
    }
    
    required init?(map: Map) {
        //Required
    }
    
    func mapping(map: Map) {
        status <- map["code"]
        message <- map["message"]
    }

	static func result<T: FullResponse>(response: IWannaServices.ServerResponse) -> T {
		if response.success && response.response["code"].intValue == 200 {
			guard let finalResponse = Mapper<T>().map(JSONString: response.response["result"].rawString()!) else { return T() }
			finalResponse.status = 200
			return finalResponse
		} else {
			return T()
		}
	}

	static func resultArray<T: FullResponse>(response: IWannaServices.ServerResponse) -> [T] {
		if response.success && response.response["code"].intValue == 200 {
			guard let finalResponse = Mapper<T>().mapArray(JSONString: response.response["result"].rawString()!) else { return [T()] }
			finalResponse.forEach({ (result) in
				result.status = 200
			})
			return finalResponse
		} else {
			return [T()]
		}
	}
}
