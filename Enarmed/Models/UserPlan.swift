//
//  UserPlan.swift
//  Enarmed
//
//  Created by Victor Alejandria on 17/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper

struct UserPlan: Mappable {

	var id: Int!
	var plan: Int!
	var name: String!
	var type: Int!
	var duration: Int!
	var questions: Int!


	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		id <- map["id"]
		name <- map["name"]
		plan <- map["plan_id"]
		type <- map["type"]
		duration <- map["duration"]
		questions <- map["cantQuestion"]
	}

}

