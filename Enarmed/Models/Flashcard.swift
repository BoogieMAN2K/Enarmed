//
//  Flashcard.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Flashcard: Mappable {
    var id: Int!
    var name: String!
    var description: String!
    var category: String!
    var files: [FlashcardFile]!
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        category <- map["category"]
        files <- map["file"]
    }
    
    static func all() -> Signal<[Flashcard], NSError> {
        let flashcardSignal = Services.requestArray(url: "flash-card/all", method: .get, returnType: [Flashcard.self])
        
        return flashcardSignal.map { $0.1 }
    }
    
    static func byId(_ id: Int) -> Signal<Flashcard, NSError> {
        let flashcardSignal = Services.requestArray(url: "flash-card/detail/\(id)", method: .get, returnType: [Flashcard.self])
        
        return flashcardSignal.map { $0.1[0] }
    }
    
    static func byCategory(_ id: Int) -> Signal<[Flashcard], NSError> {
        let flashcardSignal = Services.requestArray(url: "flash-card/category/\(id)", method: .get, returnType: [Flashcard.self])
        
        return flashcardSignal.map { $0.1 }
    }

	static func rate(_ id: Int, stars: Int) -> Signal<Bool, NSError> {
		let parameters = ["flashcard": id, "value": stars] as [String: Any]
		let flashcardSignal = try! IWannaServices.request(url: "flashcard/set-value", parameters: parameters, method: .post,
		                                             headers: ["Content-Type":"application/json", "Authorization": "Bearer \(User().userToken!)"])

		return flashcardSignal.map { $0.success }
	}

	static func recoverValue(_ id: Int!) -> Signal<Int, NSError> {
		let flashcardSignal = try! IWannaServices.request(url: "flashcard/get-value/\(id)", method: .get,
		                                                  headers: ["Content-Type":"application/json", "Authorization": "Bearer \(User().userToken!)"])

		return flashcardSignal.map { $0.response["result"].intValue }
	}
}
