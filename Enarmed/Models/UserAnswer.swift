//
//  UserAnswer.swift
//  Enarmed
//
//  Created by Victor Alejandria on 25/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct UserAnswer: Mappable {
	var quiz: String!
	var question: String!
	var answer: String!
	var valid: Bool!

	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		quiz <- map["quiz"]
		question <- map["question"]
		answer <- map["answer"]
		valid <- map["valid"]
	}
}
