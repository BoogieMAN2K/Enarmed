//
//  Statistics.swift
//  Enarmed
//
//  Created by Victor Alejandria on 25/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Statistics: Mappable {
	var exam_true: Int!
	var exam_false: Int!
	var exam_true_percentage: Int!
	var exam_false_percentage: Int!
	var quiz_true: Int!
	var quiz_false: Int!
	var quiz_true_percentage: Int!
	var quiz_false_percentage: Int!
	var flashcard_true: Int!
	var flashcard_false: Int!
	var flashcard_true_percentage: Int!
	var flashcard_false_percentage: Int!

	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		exam_true <- map["exam.true"]
		exam_false <- map["exam.false"]
		exam_true_percentage <- map["exam.truePercentage"]
		exam_false_percentage <- map["exam.falsePercentage"]
		quiz_true <- map["quiz.true"]
		quiz_false <- map["quiz.false"]
		quiz_true_percentage <- map["quiz.truePercentage"]
		quiz_false_percentage <- map["quiz.falsePercentage"]
		flashcard_true <- map["flashcard.true"]
		flashcard_false <- map["flashcard.false"]
		flashcard_true_percentage <- map["flashcard.truePercentage"]
		flashcard_false_percentage <- map["flashcard.falsePercentage"]
	}

	static func get() -> Signal<Statistics, NSError> {
		let statisticsSignal = Services.request(url: "get-stadistic", method: .get, returnType: Statistics.self)

		return statisticsSignal.map { $0.1 }
	}
}
