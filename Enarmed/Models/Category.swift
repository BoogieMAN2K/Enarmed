//
//  Category.swift
//  EnarMed
//
//  Created by Victor Alejandria on 10/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

final class Category: Mappable {

    var id: Int!
    var name: String!
    var parent: Int!
    var parentName: String!
    var childrens: [Category]!
	var flashcard: String!
    
	/// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
	init?(map: Map) {
	}

	func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        parent <- map["parent_id"]
        parentName <- map["parent"]
        childrens <- map["childrens"]
		flashcard <- map["flashcard"]
    }
    
    static func all() -> Signal<[Category], NSError> {
		let categorySignal = try! IWannaServices.request(url: "category/all", method: .get, headers: ["Authorization": "Bearer \(User().userToken!)"])

		return categorySignal.map { result in
			self.resultArray(response: result)
		}
    }
    
    static func byId(id: Int) throws -> Signal<Category, NSError> {
		let categorySignal = try! IWannaServices.request(url: "category/\(id)", method: .get, headers: ["Authorization": "Bearer \(User().userToken!)"])

		return categorySignal.map { result in
			self.result(response: result)
		}
    }
    
}
