//
//  LoginResponse.swift
//  EnarMed
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper

struct LoginResponse: Mappable {

    var token: String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        token <- map["Token"]
    }
}
