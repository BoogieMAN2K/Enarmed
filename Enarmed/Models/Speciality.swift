//
//  Speciality.swift
//  EnarMed
//
//  Created by Victor Alejandria on 9/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Speciality: Mappable {
    var id: Int!
    var name: String!
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
    
    static func all() -> Signal<[Speciality], NSError> {
        let specialitySignal = Services.requestArray(url: "specialty/all", method: .get, headers: ["Content-Type":"application/json"], returnType: [Speciality.self])

        return specialitySignal.map { $0.1 }
    }
    
    static func byId(id: Int) throws -> Signal<Speciality, NSError> {
        let specialitySignal = Services.request(url: "speciality/\(id)", method: .get, returnType: Speciality.self)
        
        return specialitySignal.map { $0.1 }
    }
    
}
