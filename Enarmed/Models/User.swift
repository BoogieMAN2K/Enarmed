//
//  User.swift
//  EnarMed
//
//  Created by Victor Alejandria on 29/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct User: Mappable {

	var userToken: String!
	var id: Int!
	var name: String!
	var email: String!
	var phone: String!
	var birthday: String!
	var speciality: Int!
	var enarmCode: String!
	var password: String!
	var lastLogin: String!
	var created: String!
	var image: String!
	var status: UserStatus!

	enum UserStatus: String {
		case activo = "Activo"
		case inactivo = "Inactivo"
		case bloqueado = "Bloqueado"
	}

	init() {
		let userDefaults = UserDefaults.standard
		if userDefaults.value(forKey: "token") == nil { return }
		self.id = userDefaults.integer(forKey: "id")
		self.userToken = userDefaults.string(forKey: "token")
	}

	init?(map: Map) {
		//Required
	}

	mutating func mapping(map: Map) {
		userToken <- map["token"]
		id <- map["id"]
		name <- map["fullname"]
		name <- map["_fulname"]
		email <- map["email"]
		email <- map["_email"]
		birthday <- map["birthday.date"]
		birthday <- map["_birthday.date"]
		speciality <- map["specialty"]
		speciality <- map["_specialty"]
		enarmCode <- map["codeEnar"]
		enarmCode <- map["_codeEnar"]
		phone <- map["phone"]
		phone <- map["_phone"]
		password <- map["password"]
		image <- map["image"]
		lastLogin <- map["lastLogin.date"]
		created <- map["createdAt.date"]
		status <- (map["enabled"], EnumTransform<UserStatus>())
	}

	static func create(user: User) -> Signal1<(Bool, String)> {
		let parameters = ["_fullname": user.name, "_email": user.email, "_password": user.password, "_specialty": user.speciality,
		                  "_phone": user.phone, "_birthday": Date.init(fromString: user.birthday, format: "dd/MM/yyyy")?.toString(format: "yyyy-MM-dd") ?? ""] as [String: Any]

		let userSignal = try! IWannaServices.request(url: "register", parameters: parameters, method: .post,
		                                        headers: ["Content-Type": "application/json"])


		let create = Signal1<(Bool, String)> { observer in
			_ = userSignal.observeNext { (result) in
				if (result.response["message"].stringValue == "Usuario registrado exitosamente, debe activar su cuenta a travez de un enlace enviado a su email.") {
					observer.next((true, result.response["message"].stringValue))
				} else {
					observer.next((false, result.response["message"].stringValue))
				}
				observer.completed()
			}

			return NonDisposable.instance
		}

		return create
	}

	static func update(user: User) throws -> Signal<Bool, NSError> {
		let parameters = ["_fullname": user.name, "_email": user.email, "_phone": user.phone, "_specialty": user.speciality,
		                  "_birthday": Date.init(fromString: user.birthday, format: "dd/MM/yyyy")?.toString(format: "yyyy-MM-dd") ?? "", "_codeenar": user.enarmCode] as [String:Any]
		let userResponse = Services.request(url: "account/update-information", parameters: parameters,
		                                    method: .post, returnType: User.self)

		let userDefaults = UserDefaults.standard
		userDefaults.set(user.name, forKey: "name")
		userDefaults.set(user.phone, forKey: "phone")
		userDefaults.set(user.email, forKey: "email")
		do {
			_ = try Speciality.byId(id: user.speciality).observeNext { (result) in
				userDefaults.set(result, forKey: "speciality")
			}
		} catch {
			userDefaults.set(user.speciality, forKey: "speciality")
		}
		userDefaults.set(user.birthday, forKey: "birthday")
		userDefaults.set(user.enarmCode, forKey: "enarCode")

		return userResponse.map { $0.0 }
	}

	static func updatePassword(password: String) -> Signal<Bool, NSError> {
		let parameters = ["password":password] as [String:Any]
		let userResponse = Services.request(url: "account/update-password", parameters: parameters, method: .post,
		                                    returnType: User.self)

		return userResponse.map { $0.0 }
	}

	static func updateProfilePicture(picture: UIImage) -> Signal<Bool, NSError> {
		let temp = picture.toBase64(size: CGSize(width: 800, height: 800))
		let parameters = ["image" : "data: image/jpeg;base64,\(temp)"]
		let	userResponse = try! IWannaServices.request(url: "account/update-image", parameters: parameters, method: .post,
		   	                                           headers: ["Content-Type":"application/json", "Authorization": "Bearer \(User().userToken!)"])

		return userResponse.map { result in
			result.response["code"].intValue == 200
		}
	}

	static func recoverPassword(email: String) -> Signal<Bool, NSError> {
		let parameters = ["email":email] as [String:Any]
		let userResponse = Services.request(url: "account/reset-password", parameters: parameters, method: .post,
		                                    returnType: User.self)

		return userResponse.map { $0.0 }
	}

	static func login(username: String, password: String) -> Signal1<Bool> {
		let loginCheck = User.checkLogin(username: username, password: password)
		let login = Signal1<Bool> { observer in
			_ = loginCheck.observeNext { (result) in
				if result {
					observer.next(true)
				} else {
					observer.next(false)
				}
				observer.completed()
			}

			return NonDisposable.instance
		}

		return login
	}

	static func info() -> Signal<User, NSError> {
		let userResponse = Services.request(url: "account/all", method: .get, returnType: UserResponse.self)

		return userResponse.map {
			var user = $0.1.user[0]
			let speciality = $0.1.speciality[0].id
			user.speciality = speciality!

			let userDefaults = UserDefaults.standard
			userDefaults.set(user.name, forKey: "name")
			userDefaults.set(user.phone, forKey: "phone")
			userDefaults.set(user.email, forKey: "email")
			userDefaults.set(user.id, forKey: "id")
			userDefaults.set(user.image, forKey: "image")
			userDefaults.set(user.lastLogin, forKey: "lastLogin")
			userDefaults.set(user.status.rawValue, forKey: "status")
			do {
				_ = try Speciality.byId(id: user.speciality).observeNext { (result) in
					userDefaults.set(result, forKey: "speciality")
				}
			} catch {
				userDefaults.set(user.speciality, forKey: "speciality")
			}
			userDefaults.synchronize()

			return user
		}
	}

	static func checkLogin(username: String, password: String ) -> Signal<Bool, NSError> {
		let parameters = ["_username" : username, "_password" : password] as [String:Any]
		let userSignal = Services.request(url: "login_check", parameters: parameters, method: .post,
		                                  headers: ["Content-Type":"application/json"], returnType: Token.self)

		return userSignal.map {
			if $0.1.token == nil {
				UserDefaults.standard.set(false, forKey: "hasLoginKey")
				return false
			}

			UserDefaults.standard.setValue($0.1.token!, forKey: "token")
			return $0.0
		}
	}

	static func userPlan() -> Signal<UserPlan, NSError> {
		let userSignal = Services.requestArray(url: "account/plan", method: .get, returnType: [UserPlan.self])

		return userSignal.map {
			if $0.1[0].id == nil {
				return UserPlan()
			}

			return $0.1[0]
		}

	}
}
