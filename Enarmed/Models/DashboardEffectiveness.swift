//
//  DashboardEfectiveness.swift
//  Enarmed
//
//  Created by Victor Alejandria on 17/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

final class DashboardEffectiveness: FullResponse {

	var valid: Int!
	var invalid: Int!
	var tries: Int!
	var promValid: Int!
	var promInvalid: Int!
	var promEffectiveness: Int!

	override func mapping(map: Map) {
		valid <- map["valid"]
		invalid <- map["invalid"]
		tries <- map["intentos"]
		promValid <- map["validProm"]
		promInvalid <- map["invalidProm"]
		promEffectiveness <- map["efectividadProm"]
	}

	static func all() -> Signal<DashboardEffectiveness, NSError> {
		let dashboardSignal = try! IWannaServices.request(url: "account/dashboard/hits", method: .get,
		                                                  headers: ["Authorization": "Bearer \(User().userToken!)"])

		return dashboardSignal.map { result in
			self.result(response: result)
		}
	}
	
}
