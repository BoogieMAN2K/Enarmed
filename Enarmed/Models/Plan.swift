//
//  Plan.swift
//  EnarMed
//
//  Created by Victor Alejandria on 9/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

final class Plan: FullResponse {
    var id: Int!
    var name: String!
	var freeQuestions: Int!
	var premiunQuestions: Int!
    
	override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
		name <- map["userPlan"]
		freeQuestions <- map["plans.free"]
		premiunQuestions <- map["plans.premium"]
    }

	static func buyFull(refPayment: String, amount: Double) -> Signal<Bool, NSError> {
		let parameters = ["status": true, "id_payment": UserDefaults.standard.string(forKey: "name") ?? "", "ref_payment": refPayment,
		                  "id_card": "", "type_card": "", "brand_card": "In-App", "number_card": "", "amount": amount.toString] as [String:Any]
		let buySignal = try! IWannaServices.request(url: "plan/generate", parameters: parameters, method: .post,
		                                                  headers: ["Authorization": "Bearer \(User().userToken!)"])

		return buySignal.map { result in
			result.success
		}
	}

}

