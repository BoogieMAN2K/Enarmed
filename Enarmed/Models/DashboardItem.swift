//
//  DashboardItem.swift
//  Enarmed
//
//  Created by Victor Alejandria on 6/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct DashboardItem: Mappable {
	var category: String!
	var result: Int!

	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		category <- map["category"]
		result <- map["result"]
	}

	static func all() -> Signal<[DashboardItem], NSError> {
		let dashboardItemSignal = Services.requestArray(url: "account/dashboard", method: .get,
		                                                returnType: [DashboardItem.self])

		return dashboardItemSignal.map { $0.1 }
	}

}

