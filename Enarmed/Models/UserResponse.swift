//
//  UserResponse.swift
//  EnarMed
//
//  Created by Victor Alejandria on 1/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper

struct UserResponse: Mappable {

    var user: [User]!
    var plan: [Plan]!
    var speciality: [Speciality]!
    
    init() {
        
    }
    
    init?(map: Map){
        
    }
    
    mutating func mapping(map: Map) {
        user <- map["user"]
        plan <- map["plan"]
        speciality <- map["specialty"]
    }
    
}
