//
//  Token.swift
//  EnarMed
//
//  Created by Victor Alejandria on 24/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import ObjectMapper

struct Token: Mappable {
    
    var token: String!
    var refreshToken: String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        token <- map["token"]
        refreshToken <- map["refresh_token"]
    }
}
