//
//  Questions.swift
//  Enarmed
//
//  Created by Victor Alejandria on 24/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

import ObjectMapper
import ReactiveKit
import IWCocoa

struct Question: Mappable {
	var id: Int!
	var question: String!
	var feedback: String!
	var answers: [Answer]!

	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		id <- map["id"]
		question <- map["question"]
		answers <- map["answer"]
		feedback <- map["feedback"]
	}

}
