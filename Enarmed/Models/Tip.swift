//
//  Tip.swift
//  EnarMed
//
//  Created by Victor Alejandria on 18/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import IWCocoa
import ReactiveKit
import ObjectMapper

struct Tip: Mappable {

	var id: Int!
	var name: String!
	var description: String!
	var categoryId: Int!
	var category: String!

	init() {

	}

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {
		id <- map["id"]
		name <- map["name"]
		description <- map["description"]
		categoryId <- map["category_id"]
		category <- map["category"]
	}

	static func all() -> Signal<[Tip], NSError> {
		let tipsSignal = Services.requestArray(url: "tips/all", method: .get, returnType: [Tip.self])

		return tipsSignal.map { $0.1 }
	}

	static func byId(id: Int) throws -> Signal<Tip, NSError> {
		let tipSignal = Services.request(url: "tips/\(id)", method: .get, returnType: Tip.self)

		return tipSignal.map { $0.1 }
	}

}
